class User {
	constructor(saveUser) {
		this.saveUser = saveUser;
	}
	async save(event, callback) {
		try {
			const user = {
				userId: event.userName,
				firstName: event.request.userAttributes.given_name,
				lastName: event.request.userAttributes.family_name,
				email: event.request.userAttributes.email,
				phoneNumber: event.request.userAttributes.phone_number,
			};
			await this.saveUser(user);
			console.log('saved user', user);
			callback(null, event);
		} catch (error) {
			console.log('save new user error', error);
			callback(error, event);
		}
	}
}

export default User;
