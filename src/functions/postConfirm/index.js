import { saveUser } from '../../libs/dynamodb';
import User from './User';

const user = new User(saveUser);

export const handler = (event, context, callback) => {
	console.log('Received event {}', JSON.stringify(event, null, 2));
	user.save(event, callback);
};
