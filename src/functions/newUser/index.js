import { signUp } from '../../libs/cognito';
import { saveUser } from '../../libs/dynamodb';
import User from './User';

const user = new User(signUp, saveUser);

export default (event, callback) => {
	console.log('newUser invoked');
	const {
		arguments: { firstName, lastName, email, phoneNumber, password },
		identity: { cognitoIdentityId },
	} = event;
	user.save(
		{
			username: cognitoIdentityId,
			firstName,
			lastName,
			email,
			phoneNumber,
			password,
		},
		callback
	);
};
