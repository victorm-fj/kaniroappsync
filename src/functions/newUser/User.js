class User {
	constructor(signUp, saveUser) {
		this.signUp = signUp;
		this.saveUser = saveUser;
	}
	async save(
		{ username, firstName, lastName, email, phoneNumber, password },
		callback
	) {
		try {
			const userAttributes = [
				{ Name: 'given_name', Value: firstName },
				{ Name: 'family_name', Value: lastName },
				{ Name: 'email', Value: email },
				{ Name: 'phone_number', Value: phoneNumber },
			];
			const params = {
				ClientId: process.env.COGNITO_APP_CLIENT_ID,
				Password: password,
				Username: username,
				UserAttributes: userAttributes,
			};
			await this.signUp(params);
			const newUser = {
				userId: username,
				firstName,
				lastName,
				email,
				phoneNumber,
			};
			await this.saveUser(newUser);
			callback(null, newUser);
		} catch (error) {
			console.log('save new user error', error);
			callback(error, null);
		}
	}
}

export default User;
