import { sendMessage } from '../../libs/sns';
import { sendEmails } from '../../libs/ses';
import Invites from './Invites';

const invites = new Invites(sendMessage, sendEmails);

export default (event, callback) => {
	console.log('inviteFriends Fn invoked');
	// Data from the 'invite' mutation event
	const {
		arguments: { friends, downloadURL, sendBy },
		// identity: { cognitoIdentityId },
	} = event;
	invites.send(friends, downloadURL, sendBy, callback);
};
