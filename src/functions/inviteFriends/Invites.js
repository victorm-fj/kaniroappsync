class Invites {
	constructor(sendMessage, sendEmails) {
		this.sendMessage = sendMessage;
		this.sendEmails = sendEmails;
	}
	async send(friends, downloadURL, sendBy, callback) {
		try {
			let results = null;
			if (sendBy === 'sms') {
				results = await Promise.all(
					friends.map(friend =>
						this.sendMessage(friend.phoneNumber, downloadURL)
					)
				);
				console.log("successfully sent SMS's to friends", results);
			} else {
				const emails = friends.reduce((acc, obj) => [...acc, obj.email], []);
				results = await this.sendEmails(emails, downloadURL);
				console.log('successfully sent emails to friends', results);
			}
			callback(null, friends);
		} catch (error) {
			console.log('error sending invitation to friends', error);
			callback(error, null);
		}
	}
}

export default Invites;
