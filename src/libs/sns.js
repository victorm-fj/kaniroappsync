const AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });

export const call = (action, params) => {
	const sns = new AWS.SNS();
	return sns[action](params).promise();
};

export const sendMessage = (phoneNumber, downloadURL) => {
	const params = {
		Message: `You have been invited to join Kaniro, your pet's managament app. Download the app here ${downloadURL}`,
		MessageAttributes: {
			'AWS.SNS.SMS.SenderID': {
				DataType: 'String',
				StringValue: 'Kaniro',
			},
			'AWS.SNS.SMS.SMSType': {
				DataType: 'String',
				StringValue: 'Transactional',
			},
		},
		PhoneNumber: phoneNumber,
	};
	return call('publish', params);
};
