const AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });

export const call = (action, params) => {
	const dynamoDB = new AWS.DynamoDB.DocumentClient();
	return dynamoDB[action](params).promise();
};

export const saveUser = item => {
	const params = {
		TableName: process.env.USERS_TABLE,
		Item: item,
	};
	return call('put', params);
};

export const deleteUser = userId => {
	const params = {
		TableName: process.env.USERS_TABLE,
		Key: { userId },
	};
	return call('delete', params);
};

export const getUser = userId => {
	const params = {
		TableName: process.env.USERS_TABLE,
		Key: { userId },
	};
	return call('get', params);
};
