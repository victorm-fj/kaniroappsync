const AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });

export const call = (action, params) => {
	const ses = new AWS.SES();
	return ses[action](params).promise();
};

export const sendEmails = (emails, downloadURL) => {
	const subject = 'Kaniro app, for pets';
	const htmlBody = `
    <h1>Kaniro app invitation</h1>
    <p>You have been invited to join <strong>Kaniro</strong> app. To accept the
    invitation please click in the link below and start tracking your pets' health.</p>
    <p><a href=${downloadURL}>Accept invitation</a></p>
  `;
	const textBody =
		'This email was sent with Amazon SES using the AWS SDK for JavaScript.';
	const encoding = 'UTF-8';
	const params = {
		Destination: {
			ToAddresses: emails,
		},
		Message: {
			Body: {
				Html: {
					Charset: encoding,
					Data: htmlBody,
				},
				Text: {
					Charset: encoding,
					Data: textBody,
				},
			},
			Subject: {
				Charset: encoding,
				Data: subject,
			},
		},
		ReturnPath: 'victor@akto.io',
		Source: 'victor@akto.io',
	};
	return call('sendEmail', params);
};
