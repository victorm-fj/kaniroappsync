import inviteFriends from './functions/inviteFriends';

export const handler = (event, context, callback) => {
	console.log('Received event {}', JSON.stringify(event, null, 2));
	switch (event.field) {
	case 'inviteFriends':
		inviteFriends(event, callback);
		break;
	default:
		callback(`Unknown field, unable to resolve ${event.field}`, null);
		break;
	}
};
