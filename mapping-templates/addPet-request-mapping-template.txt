{
  "version": "2017-02-28",
  "operation": "PutItem",
  "key": {
    "userId": { "S": "${context.identity.cognitoIdentityId}" },
    "petId": { "S": "${context.arguments.petId}" }
  },
  #set($attrs = $util.dynamodb.toMapValues($context.arguments))
  #set($attrs.userId = $util.dynamodb.toDynamoDB($context.identity.cognitoIdentityId))
  "attributeValues": $util.toJson($attrs)
}