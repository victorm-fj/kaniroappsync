#set($pet = $context.result)
#if($pet.image) 
  #set($image = $util.dynamodb.fromS3ObjectJson($pet.image))
  #set($pet.image = $image)
#end
$utils.toJson($pet)


