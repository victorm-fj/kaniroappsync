{
  "version": "2017-02-28",
  "operation": "UpdateItem",
  "key": {
    "userId": { "S": "${context.identity.cognitoIdentityId}" }
  },
  "update": {
    "expression": "SET profilePic = :profilePic, signedUrl = :signedUrl",
    "expressionValues": {
      ":profilePic": { "S": "${context.arguments.profilePic}" },
      ":signedUrl": { "S": "${context.arguments.signedUrl}" }
    }
  }
}