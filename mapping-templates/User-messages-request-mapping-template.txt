{
  "version": "2017-02-28",
  "operation": "Query",
  "query": {
    "expression": "senderId = :userId",
    "expressionValues": {
      ":userId": { "S": "${context.source.userId}" }
    }
  },
  "index": "senderId-index",
  "scanIndexForward": false,
  "limit": #if(${context.arguments.first}) ${context.arguments.first} #else 20 #end,
  "nextToken": #if(${context.arguments.after}) ${context.arguments.after} #else null #end
}